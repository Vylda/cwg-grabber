// ==UserScript==
// @name		CWG grabber
// @version		1.6
// @author		Vilém Lipold
// @namespace	http://www.gcgpx.cz/cwggrabber
// @description	Stáhne logy a zjistí potřebný počet CWG
// @license		MIT License; http://www.opensource.org/licenses/mit-license.php
// @copyright	2017-24, Vilém Lipold (http://www.gcgpx.cz/)
// @updateURL	http://www.gcgpx.cz/cwggrabber/cwggrabber.meta.js
// @downloadURL	http://www.gcgpx.cz/cwggrabber/cwggrabber.user.js
// @icon 		http://www.gcgpx.cz/cwggrabber/cwggrabber.png
// @icon64		http://www.gcgpx.cz/cwggrabber/cwggrabber64.png
// @include		https://www.geocaching.com/geocache/*
// @include		https://www.geocaching.com/seek/*
// @homepage	https://gitlab.com/Vylda/cwg-grabber#jak-spustit-cwg-grabber
// @run-at		document-end
// @grant		none
// ==/UserScript==
