# Jak spustit CWG Grabber

CWG Grabber je aplikace pro vlastníky eventů, kteří chtějí mít přehled o objednaných xWG. Výstup je v tabulce nebo v XLSX souboru (Excel).
Autorem námětu na tuto aplikaci ja kačer Etigo.

## Instalace

1.  Je potřeba mít v prohlížeči nainstalovaný nějaký spouštěč skriptů.
    *  pro Firefox: [Tamper Monkey](https://addons.mozilla.org/cs/firefox/addon/tampermonkey/), [Grease Monkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
    *  pro Chrome: [Tamper Monkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)

2.  Je třeba nainstalovat [CWG Grabber](http://gcgpx.cz/cwggrabber/cwggrabber.user.js).
    *  Pokud je nainstalovaný Monkey (viz odkazy výše), dojde při kliknutí na odkaz [CWG Grabberu](http://gcgpx.cz/cwggrabber/cwggrabber.user.js) k automatickému spuštění instalace.

## Použití
1.  Po úspěšném nainstalování se při zobrazení eventu na stránkách geocaching.com objeví vlevo dole tlačítko Získat počty xWG.
2.  Po klepnutí na tlačítko Získat počty xWG se stáhnou a zpracují všechny logy (kromě publikačního) a objeví se v samostaném okně v tabulce.
3.  Pokud je vše v pořádku, uvidíte pod tabulkou tlačítko Vytvořit soubor Excelu. Po kliknutí na tlačítko se vygeneruje xlsx soubor se stejnými daty jako v tabulce a nabídne se ke stažení.
4.  V případě problémů je k dispozici pouze tlačítko Zkopírovat tabulku do schránky, které zkopíruje obsah zobrazené tabulky do systémové schránky).
5.  Přepínač pod tabulkou (jeho stav si aplikace pamatuje) slouží k tomu, aby byly vygenerovány logy každý na samostatný řádek (zapnuto) nebo všechny logy jednoho uživatele v jednom řádku (vypnuto).
6.  Pokud se něco při inicializaci či tvorbě tabulky nepovede, nabídne se (provede se) zkopírování dat z tabulky do schránky operačního systému. Její obsah pak můžete ručně vložit do jakéhokoliv textového či tabulkového editoru.

## Logování účastníků
Aby CWG grabber vytvořil co nejrelevantnější výsledky, je třeba, aby účastníci eventu do svého logu napsali počet koleček, které objednávají ve formátu **+2CWG** (nebo +2 CWG, 2xCWG či prostě 2 CWG).
Zjišťování počtu je nezávislé na velikosti písmen i na typu kolečka (projde třeba SWG, PWG, JWG…). Rozpoznány jsou i slovní varianty čísel (jedna–dvacet) a některé varianty označení xWG (kolečko, suk, dřevo). V tomto případě musí být opět dodržen výše uvedený zápis (množství čeho).

V případě, že se účastník chce z eventu odhlásit a nemá zájem o kolečka, je vhodné, aby do svého odhlašovacího logu napsal např. **-2CWG** (opět podle počtu odhlášených koleček).

### Příklad upozornění do listingu

#### text
```
CWG , prosím, objednávejte zápisem +2CWG nebo 2CWG, protože objednávky zpracuju automaticky. Pokud byste se odhlásili, dejte to vědět i v počtu CWG zápisem -2CWG. Díky.
```
#### HTML kód
```html
<p style="color:#365f0c;background-color:#bef23f;font-weight:bold;text-align:center;font-size:18px;padding:20px;margin:20px 20px 40px 20px;border:2px #365f0c solid;border-radius:10px;box-shadow:5px 5px 5px rgba(54, 95, 12, 0.75);">
CWG , prosím, objednávejte zápisem +2CWG nebo 2CWG, protože objednávky zpracuju automaticky. Pokud byste se odhlásili, dejte to vědět i v počtu CWG zápisem -2CWG. Díky.
</p>
```
Příklad na eventu [Na Zelený čtvrtek zelené pivo](https://www.geocaching.com/geocache/GC7KFZD_na-zeleny-ctvrtek-zelene-pivo).
## License
[MIT](http://www.opensource.org/licenses/mit-license.php)
