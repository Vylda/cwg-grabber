// ==UserScript==
// @name		CWG grabber
// @version		1.6
// @author		Vilém Lipold
// @namespace	http://www.gcgpx.cz/cwggrabber
// @description	Stáhne logy a zjistí potřebný počet CWG
// @license		MIT License; http://www.opensource.org/licenses/mit-license.php
// @copyright	2017-24, Vilém Lipold (http://www.gcgpx.cz/)
// @updateURL	http://www.gcgpx.cz/cwggrabber/cwggrabber.meta.js
// @downloadURL	http://www.gcgpx.cz/cwggrabber/cwggrabber.user.js
// @icon 		http://www.gcgpx.cz/cwggrabber/cwggrabber.png
// @icon64		http://www.gcgpx.cz/cwggrabber/cwggrabber64.png
// @include		https://www.geocaching.com/geocache/*
// @include		https://www.geocaching.com/seek/*
// @homepage	https://gitlab.com/Vylda/cwg-grabber#jak-spustit-cwg-grabber
// @run-at		document-end
// @grant		none
// ==/UserScript==


//https://github.com/jsegarra1971/MyExcel

(function () {
  "use strict"
  const version = GM_info.script.version;
  console.info(`Starting CWG Grabber ${version}`);

  const TIMEOUT = 7000;
  const UT_LAZY_LOAD_TRYES = 200;
  const errorStyle = 'padding: 15px; overflow: hidden; box-shadow: 0 0 15px rgba(0,0,0,0.75); z-index: 999999999; border-radius: 10px; border: 2px solid rgb(153, 0, 0); border-image: none; left: 50%; top: 10px; width: 500px; text-align: center; color: rgb(153, 0, 0); font-weight: bold; margin-left: -250px; position: fixed; background-color: rgba(255, 238, 238, 0.75); transition: all 1s;'
  const errorEndStyle = [
    { key: "top", value: "-80px" },
    { key: "width", value: "0" },
    { key: "height", value: "0" },
    { key: "marginLeft", value: "0" },
    { key: "padding", value: "0" }
  ];

  const createError = (text) => {
    const div = document.createElement("div");
    div.style.cssText = errorStyle;
    div.innerHTML = text;

    document.body.appendChild(div);

    setTimeout(function () {
      errorEndStyle.forEach(style => {
        div.style[style.key] = style.value;
      });

      setTimeout(function () {
        document.body.removeChild(div);
      }, 1500);
    }, TIMEOUT);
  }


  try {
    eval("(function() { async _ => _; })();");
  } catch (e) {
    createError("Váš prohlížeč je zastaralý a CWG Grabber v něm nebude fungovat!");
    return false;
  }

  const w = window;
  let ut = w.userToken;

  const tokenPromise = new Promise((resolve, reject) => {
    let ut = w.userToken;
    let utCounter = 0;
    if (ut) {
      resolve(ut);
    }
    let tokenInterval = setInterval(() => {
      ut = w.userToken;
      if (ut) {
        clearInterval(tokenInterval);
        resolve(ut);
      } else if (utCounter > UT_LAZY_LOAD_TRYES) {
        clearInterval(tokenInterval);
        reject();
      }
      utCounter++;
    }, 500);
  });

  const isEvent = () => {
    const use = document.querySelector("#cacheDetails a.cacheImage svg use");
    return use && (/icon\-6(\-disabled)?$/i.test(use.href.baseVal) // event
      || /icon\-13(\-disabled)?$/i.test(use.href.baseVal) // cito
      || /icon\-453(\-disabled)?$/i.test(use.href.baseVal) // megaevent
      || /icon\-7005(\-disabled)?$/i.test(use.href.baseVal) // gigaevent
      || /icon\-1304(\-disabled)?$/i.test(use.href.baseVal) // maze
      || /icon\-1304(\-disabled)?$/i.test(use.href.baseVal) // maze
      || /icon\-3653(\-disabled)?$/i.test(use.href.baseVal) // Community Celebration Events - 2020
    );
  }

  if (!isEvent()) {
    console.info("CWG Grabber: Non event listing!");
    return;
  }

  //lazyload excellentExport
  const lazyLoad = (src) => {
    return new Promise(function (resolve, reject) {
      var s;
      s = document.createElement('script');
      s.async = true;
      s.defer = true;
      s.src = src;
      s.addEventListener("load", () => {
        resolve();
      });
      s.addEventListener("error", e => {
        reject(e);
      });
      document.head.appendChild(s);
    });
  }

  let toExcel = false, copyListener = false, processedLogs = 0, progressMax = 0;

  const getLogbook = async (page) => {
    console.info(`CWG Grabber: getting logbook № ${page}.`);
    await delay();
    const xhr = new XMLHttpRequest();
    const url = `https://www.geocaching.com/seek/geocache.logbook?tkn=${ut}&idx=${page}&num=25`;

    const promise = new Promise((resolve, reject) => {
      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve(xhr.response);
        } else {
          reject(xhr.statusText);
        }
      };
      xhr.onerror = () => reject(xhr.statusText);
    });
    xhr.open("GET", url);
    xhr.send();

    return promise;
  }

  const delay = () => {
    return new Promise(resolve => setTimeout(resolve, 1000 + Math.floor(Math.random() * 3000)));
  }

  const processLogs = async () => {
    for (let i = 2; i <= totalPages; i++) {
      let data = await processLogbook(i);
      if (data.status && data.status == "success" && data.pageInfo && data.data) {
        if (totalPages != data.pageInfo.totalPages || totalLogs != data.pageInfo.totalRows) {
          start();
          break;
        }
        parseLogs(data.data);
      }
    }
    setTimeout(() => {
      createDataTable();
    }, 2500);
  }

  const processLogbook = async (page) => {
    return new Promise((resolve, reject) => {
      getLogbook(page).then(json => {
        try {
          resolve(JSON.parse(json));
        } catch (err) {
          reject(err);
        }
      }).catch(err => {
        reject(err);
      });
    });
  }

  const start = () => {
    dom.overlay.clear();
    dom.overlay.addProgress();
    processedLogs = 0;

    processLogbook(1).then(data => {
      totalPages = 0;
      totalLogs = 0;
      logs = [];
      if (data.status && data.status == "success" && data.pageInfo && data.data) {
        progressMax = data.pageInfo.totalRows;
        totalPages = data.pageInfo.totalPages;
        totalLogs = data.pageInfo.totalRows;
        parseLogs(data.data);
        processLogs();
      }
    }).catch(error => {
      console.error(error);
    });
  }

  const removeHTML = (text) => {
    dom.fragment.innerHTML = text;
    return dom.fragment.textContent.replace(/(\r?\n|\r)$/, "");
  }

  const parseLogs = (data) => {
    data.forEach(item => {
      let logExist = logs.findIndex(log => log.userName == item.UserName);
      let isPublish = (item.LogType == "Publish Listing") || (item.LogType == "Attended") || (item.LogType == "Announcement");
      if (!isPublish) {
        let text = removeHTML(item.LogText);
        if (logExist == -1) {
          logs.push({
            userName: item.UserName,
            logs: [{
              text,
              type: item.LogType,
              date: item.Created,
              cwgs: parseCWGs(text)
            }]
          });
        } else {
          logs[logExist].logs.push({
            text,
            type: item.LogType,
            date: item.Created,
            cwgs: parseCWGs(text)
          })
        }
      }
      setTimeout(() => {
        processedLogs++
        dom.overlay.setValue(Math.round(100 * processedLogs / progressMax));
      }, 50);

    });
  }

  const parseCWGs = (text) => {
    let toReplace = [
      {
        keys: ["jedno", "jeden"],
        value: 1
      },
      {
        keys: ["dve", "dva"],
        value: 2
      },
      {
        keys: ["tri"],
        value: 3
      },
      {
        keys: ["ctyri", "štyri"],
        value: 4
      },
      {
        keys: ["pet", "pat"],
        value: 5
      },
      {
        keys: ["sest"],
        value: 6
      },
      {
        keys: ["sedm", "sedum", "sedem"],
        value: 7
      },
      {
        keys: ["osm", "osum", "osem"],
        value: 8
      },
      {
        keys: ["devet", "devat"],
        value: 9
      },
      {
        keys: ["deset", "desat"],
        value: 10
      },
      {
        keys: ["jedenact", "jedenast"],
        value: 11
      },
      {
        keys: ["dvanact", "dvanast"],
        value: 12
      },
      {
        keys: ["trinact", "trinast"],
        value: 13
      },
      {
        keys: ["ctrnact", "strnast"],
        value: 14
      },
      {
        keys: ["patnact", "patnast"],
        value: 15
      },
      {
        keys: ["sestnact", "sestnast"],
        value: 16
      },
      {
        keys: ["sedmnact", "sedumnact", "sedemnast"],
        value: 17
      },
      {
        keys: ["osmmnact", "osumnact", "osemnast"],
        value: 18
      },
      {
        keys: ["devatenact", "devatnast"],
        value: 19
      },
      {
        keys: ["dvacet", "dvatsat"],
        value: 20
      },
      {
        keys: [
          "kolecko[^a-z]",
          "koliesko[^a-z]",
          "drevo[^a-z]",
          "dreva[^a-z]",
          "drivko[^a-z]",
          "drivka[^a-z]",
          "drievko[^a-z]",
          "drievka[^a-z]",
          "kolisko[^a-z]",
          "koliska[^a-z]",
          "kolco[^a-z]",
          "kolca[^a-z]",
          "kolecka[^a-z]",
          "kolecek[^a-z]",
          "kolieska[^a-z]",
          "koliesok[^a-z]",
          "suk[^a-z]",
          "suky[^a-z]",
          "drievie[^a-z]",
          "puk[^a-z]",
          "puky[^a-z]"
        ],
        value: "CWG"
      },
      {
        keys: [
          "([^\\dx×* ])\\s*\\wWG"
        ],
        value: "$1+1CWG"
      }
    ];
    let noaccText = removeAccent(text);

    let reCwg = /\s*([\+\-]?\s*\d+)\s*[x×*]?\s*\wWG/gmi;

    // mám exact match
    if (!reCwg.test(noaccText)) {
      toReplace.forEach(replaceElement => {
        replaceElement.keys.forEach(replaceText => {
          let reg = new RegExp(replaceText, "gmi");
          noaccText = noaccText.replace(reg, replaceElement.value);
        })
      });
    }
    let cwgs = noaccText.match(reCwg);
    let cwgsSum = 0;
    if (cwgs) {
      cwgs.forEach(cwg => {
        let num = parseInt(cwg.replace(/\wWG/gi, "").trim());
        if (num) {
          cwgsSum += num;
        }
      });
    }
    return cwgsSum;
  }

  const removeAccent = (string) => {
    //https://github.com/tyxla/remove-accents/blob/master/index.js
    let characterMap = {
      "À": "A",
      "Á": "A",
      "Â": "A",
      "Ã": "A",
      "Ä": "A",
      "Å": "A",
      "Ấ": "A",
      "Ắ": "A",
      "Ẳ": "A",
      "Ẵ": "A",
      "Ặ": "A",
      "Æ": "AE",
      "Ầ": "A",
      "Ằ": "A",
      "Ȃ": "A",
      "Ç": "C",
      "Ḉ": "C",
      "È": "E",
      "É": "E",
      "Ê": "E",
      "Ë": "E",
      "Ế": "E",
      "Ḗ": "E",
      "Ề": "E",
      "Ḕ": "E",
      "Ḝ": "E",
      "Ȇ": "E",
      "Ì": "I",
      "Í": "I",
      "Î": "I",
      "Ï": "I",
      "Ḯ": "I",
      "Ȋ": "I",
      "Ð": "D",
      "Ñ": "N",
      "Ò": "O",
      "Ó": "O",
      "Ô": "O",
      "Õ": "O",
      "Ö": "O",
      "Ø": "O",
      "Ố": "O",
      "Ṍ": "O",
      "Ṓ": "O",
      "Ȏ": "O",
      "Ù": "U",
      "Ú": "U",
      "Û": "U",
      "Ü": "U",
      "Ý": "Y",
      "à": "a",
      "á": "a",
      "â": "a",
      "ã": "a",
      "ä": "a",
      "å": "a",
      "ấ": "a",
      "ắ": "a",
      "ẳ": "a",
      "ẵ": "a",
      "ặ": "a",
      "æ": "ae",
      "ầ": "a",
      "ằ": "a",
      "ȃ": "a",
      "ç": "c",
      "ḉ": "c",
      "è": "e",
      "é": "e",
      "ê": "e",
      "ë": "e",
      "ế": "e",
      "ḗ": "e",
      "ề": "e",
      "ḕ": "e",
      "ḝ": "e",
      "ȇ": "e",
      "ì": "i",
      "í": "i",
      "î": "i",
      "ï": "i",
      "ḯ": "i",
      "ȋ": "i",
      "ð": "d",
      "ñ": "n",
      "ò": "o",
      "ó": "o",
      "ô": "o",
      "õ": "o",
      "ö": "o",
      "ø": "o",
      "ố": "o",
      "ṍ": "o",
      "ṓ": "o",
      "ȏ": "o",
      "ù": "u",
      "ú": "u",
      "û": "u",
      "ü": "u",
      "ý": "y",
      "ÿ": "y",
      "Ā": "A",
      "ā": "a",
      "Ă": "A",
      "ă": "a",
      "Ą": "A",
      "ą": "a",
      "Ć": "C",
      "ć": "c",
      "Ĉ": "C",
      "ĉ": "c",
      "Ċ": "C",
      "ċ": "c",
      "Č": "C",
      "č": "c",
      "C̆": "C",
      "c̆": "c",
      "Ď": "D",
      "ď": "d",
      "Đ": "D",
      "đ": "d",
      "Ē": "E",
      "ē": "e",
      "Ĕ": "E",
      "ĕ": "e",
      "Ė": "E",
      "ė": "e",
      "Ę": "E",
      "ę": "e",
      "Ě": "E",
      "ě": "e",
      "Ĝ": "G",
      "Ǵ": "G",
      "ĝ": "g",
      "ǵ": "g",
      "Ğ": "G",
      "ğ": "g",
      "Ġ": "G",
      "ġ": "g",
      "Ģ": "G",
      "ģ": "g",
      "Ĥ": "H",
      "ĥ": "h",
      "Ħ": "H",
      "ħ": "h",
      "Ḫ": "H",
      "ḫ": "h",
      "Ĩ": "I",
      "ĩ": "i",
      "Ī": "I",
      "ī": "i",
      "Ĭ": "I",
      "ĭ": "i",
      "Į": "I",
      "į": "i",
      "İ": "I",
      "ı": "i",
      "Ĳ": "IJ",
      "ĳ": "ij",
      "Ĵ": "J",
      "ĵ": "j",
      "Ķ": "K",
      "ķ": "k",
      "Ḱ": "K",
      "ḱ": "k",
      "K̆": "K",
      "k̆": "k",
      "Ĺ": "L",
      "ĺ": "l",
      "Ļ": "L",
      "ļ": "l",
      "Ľ": "L",
      "ľ": "l",
      "Ŀ": "L",
      "ŀ": "l",
      "Ł": "l",
      "ł": "l",
      "Ḿ": "M",
      "ḿ": "m",
      "M̆": "M",
      "m̆": "m",
      "Ń": "N",
      "ń": "n",
      "Ņ": "N",
      "ņ": "n",
      "Ň": "N",
      "ň": "n",
      "ŉ": "n",
      "N̆": "N",
      "n̆": "n",
      "Ō": "O",
      "ō": "o",
      "Ŏ": "O",
      "ŏ": "o",
      "Ő": "O",
      "ő": "o",
      "Œ": "OE",
      "œ": "oe",
      "P̆": "P",
      "p̆": "p",
      "Ŕ": "R",
      "ŕ": "r",
      "Ŗ": "R",
      "ŗ": "r",
      "Ř": "R",
      "ř": "r",
      "R̆": "R",
      "r̆": "r",
      "Ȓ": "R",
      "ȓ": "r",
      "Ś": "S",
      "ś": "s",
      "Ŝ": "S",
      "ŝ": "s",
      "Ş": "S",
      "Ș": "S",
      "ș": "s",
      "ş": "s",
      "Š": "S",
      "š": "s",
      "Ţ": "T",
      "ţ": "t",
      "ț": "t",
      "Ț": "T",
      "Ť": "T",
      "ť": "t",
      "Ŧ": "T",
      "ŧ": "t",
      "T̆": "T",
      "t̆": "t",
      "Ũ": "U",
      "ũ": "u",
      "Ū": "U",
      "ū": "u",
      "Ŭ": "U",
      "ŭ": "u",
      "Ů": "U",
      "ů": "u",
      "Ű": "U",
      "ű": "u",
      "Ų": "U",
      "ų": "u",
      "Ȗ": "U",
      "ȗ": "u",
      "V̆": "V",
      "v̆": "v",
      "Ŵ": "W",
      "ŵ": "w",
      "Ẃ": "W",
      "ẃ": "w",
      "X̆": "X",
      "x̆": "x",
      "Ŷ": "Y",
      "ŷ": "y",
      "Ÿ": "Y",
      "Y̆": "Y",
      "y̆": "y",
      "Ź": "Z",
      "ź": "z",
      "Ż": "Z",
      "ż": "z",
      "Ž": "Z",
      "ž": "z",
      "ſ": "s",
      "ƒ": "f",
      "Ơ": "O",
      "ơ": "o",
      "Ư": "U",
      "ư": "u",
      "Ǎ": "A",
      "ǎ": "a",
      "Ǐ": "I",
      "ǐ": "i",
      "Ǒ": "O",
      "ǒ": "o",
      "Ǔ": "U",
      "ǔ": "u",
      "Ǖ": "U",
      "ǖ": "u",
      "Ǘ": "U",
      "ǘ": "u",
      "Ǚ": "U",
      "ǚ": "u",
      "Ǜ": "U",
      "ǜ": "u",
      "Ứ": "U",
      "ứ": "u",
      "Ṹ": "U",
      "ṹ": "u",
      "Ǻ": "A",
      "ǻ": "a",
      "Ǽ": "AE",
      "ǽ": "ae",
      "Ǿ": "O",
      "ǿ": "o",
      "Þ": "TH",
      "þ": "th",
      "Ṕ": "P",
      "ṕ": "p",
      "Ṥ": "S",
      "ṥ": "s",
      "X́": "X",
      "x́": "x",
      "Ѓ": "Г",
      "ѓ": "г",
      "Ќ": "К",
      "ќ": "к",
      "A̋": "A",
      "a̋": "a",
      "E̋": "E",
      "e̋": "e",
      "I̋": "I",
      "i̋": "i",
      "Ǹ": "N",
      "ǹ": "n",
      "Ồ": "O",
      "ồ": "o",
      "Ṑ": "O",
      "ṑ": "o",
      "Ừ": "U",
      "ừ": "u",
      "Ẁ": "W",
      "ẁ": "w",
      "Ỳ": "Y",
      "ỳ": "y",
      "Ȁ": "A",
      "ȁ": "a",
      "Ȅ": "E",
      "ȅ": "e",
      "Ȉ": "I",
      "ȉ": "i",
      "Ȍ": "O",
      "ȍ": "o",
      "Ȑ": "R",
      "ȑ": "r",
      "Ȕ": "U",
      "ȕ": "u",
      "B̌": "B",
      "b̌": "b",
      "Č̣": "C",
      "č̣": "c",
      "Ê̌": "E",
      "ê̌": "e",
      "F̌": "F",
      "f̌": "f",
      "Ǧ": "G",
      "ǧ": "g",
      "Ȟ": "H",
      "ȟ": "h",
      "J̌": "J",
      "ǰ": "j",
      "Ǩ": "K",
      "ǩ": "k",
      "M̌": "M",
      "m̌": "m",
      "P̌": "P",
      "p̌": "p",
      "Q̌": "Q",
      "q̌": "q",
      "Ř̩": "R",
      "ř̩": "r",
      "Ṧ": "S",
      "ṧ": "s",
      "V̌": "V",
      "v̌": "v",
      "W̌": "W",
      "w̌": "w",
      "X̌": "X",
      "x̌": "x",
      "Y̌": "Y",
      "y̌": "y",
      "A̧": "A",
      "a̧": "a",
      "B̧": "B",
      "b̧": "b",
      "Ḑ": "D",
      "ḑ": "d",
      "Ȩ": "E",
      "ȩ": "e",
      "Ɛ̧": "E",
      "ɛ̧": "e",
      "Ḩ": "H",
      "ḩ": "h",
      "I̧": "I",
      "i̧": "i",
      "Ɨ̧": "I",
      "ɨ̧": "i",
      "M̧": "M",
      "m̧": "m",
      "O̧": "O",
      "o̧": "o",
      "Q̧": "Q",
      "q̧": "q",
      "U̧": "U",
      "u̧": "u",
      "X̧": "X",
      "x̧": "x",
      "Z̧": "Z",
      "z̧": "z",
    };

    let chars = Object.keys(characterMap).join('|');
    let allAccents = new RegExp(chars, 'g');
    return string.replace(allAccents, match => characterMap[match]);
  }

  const addCopyListener = (button) => {
    if (!copyListener) {
      copyListener = true;
      button.addEventListener("click", () => {
        let table = document.querySelector("#cwgGrabberTable");
        if (table) {
          if (toExcel && w.$JExcel) {
            createExcel(logs);
          } else {
            dom.overlay.copyToClipboard(table);
            alert("Tabulka zkopírována do schránky");
          }
        } else {
          alert("Chyba: nelze zkopírovat tabulku!");
        }
      });
    }
  }

  const createOverlay = () => {
    const container = document.createElement("div");
    container.id = "cwgGrabberOverlayContainer";
    let myTable = null;

    const setValue = function (value) {
      meter.style.width = `${value}%`;
    }

    const body = document.createElement("div");
    body.id = "cwgGrabberOverlayBody";
    container.appendChild(body);

    const progress = document.createElement("div");
    progress.classList.add("meter");
    progress.classList.add("animate");
    const meter = document.createElement("span");
    meter.appendChild(document.createElement("span"));
    progress.appendChild(meter);
    setValue(0);

    const head = document.createElement("h3");

    const closeBtn = document.createElement("div");
    closeBtn.textContent = "×";
    closeBtn.classList.add("close-button");
    closeBtn.addEventListener("click", () => {
      close();
    });

    const copyBtn = document.createElement("button");
    copyBtn.textContent = toExcel ? "Vytvořit soubor Excelu." : "Zkopírovat tabulku do schránky.";
    copyBtn.classList.add("copy-button");

    const switcher = document.createElement("div");
    switcher.classList.add("onoffswitch");
    const swInput = document.createElement("input");
    swInput.type = "checkbox";
    swInput.name = "singleLinesSwitch";
    swInput.id = "singleLinesSwitch";
    swInput.classList.add("onoffswitch-checkbox");

    const storedValue = w.localStorage.getItem("CwgGgrabberSwitchState");
    swInput.checked = storedValue == "true" ? true : false;
    swInput.addEventListener("change", () => {
      w.localStorage.setItem("CwgGgrabberSwitchState", swInput.checked);
    });

    const swLabel = document.createElement("label");
    swLabel.classList.add("onoffswitch-label");
    swLabel.setAttribute("for", "singleLinesSwitch");
    const swSpan1 = document.createElement("span");
    swSpan1.classList.add("onoffswitch-inner");
    const swSpan2 = document.createElement("span");
    swSpan2.classList.add("onoffswitch-switch");
    swLabel.appendChild(swSpan1);
    swLabel.appendChild(swSpan2);
    switcher.appendChild(swInput);
    switcher.appendChild(swLabel);

    const swTextLabel = document.createElement("label");
    swTextLabel.classList.add("textlabel");
    swTextLabel.setAttribute("for", "singleLinesSwitch");
    swTextLabel.textContent = "Logy na samostaných řádcích:";

    const clear = function () {
      body.innerHTML = "";
      body.classList.remove("table");
    };

    const addProgress = function () {
      clear();
      setValue(0);
      head.textContent = "Stahuji logy z logbooku"
      body.appendChild(head)
      body.appendChild(progress);
      if (!document.body.contains(container)) {
        document.body.appendChild(container);
      }
    };

    const close = function () {
      if (document.body.contains(container)) {
        document.body.removeChild(container);
      }
      let oldAnchor;
      while (oldAnchor = document.querySelector("cwgDownload")) {
        document.body.removeChild(oldAnchor);
      }

      logs = [];
      dom.getLogs.disabled = false;
    };

    const showTable = function (table) {
      clear();
      myTable = table;
      body.classList.add("table");
      head.textContent = "Logy z logbooku staženy"
      body.appendChild(head)
      const div = document.createElement("div");
      div.classList.add("tableContainer");
      div.appendChild(table)
      body.appendChild(div);
      body.appendChild(closeBtn);

      const lastLine = document.createElement("div");
      lastLine.classList.add("cwgGrabberLastLine");


      if (toExcel) {
        lastLine.appendChild(swTextLabel);
        lastLine.appendChild(switcher);
      }
      lastLine.appendChild(copyBtn);
      body.appendChild(lastLine);

      if (!document.body.contains(container)) {
        document.body.appendChild(container);
      }

      addCopyListener(copyBtn);
    }

    function copyToClipboard(elem) {
      var targetId = "_hiddenCopyText_";
      var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
      var origSelectionStart, origSelectionEnd;
      if (isInput) {
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
      } else {
        target = document.getElementById(targetId);
        if (!target) {
          var target = document.createElement("textarea");
          target.style.position = "absolute";
          target.style.left = "-9999999px";
          target.style.top = "0";
          target.style.zIndex = "-1";
          target.id = targetId;
          document.body.appendChild(target);
        }
        let text = elem.innerHTML.replace(/\n/g, "").replace(/<\/tr>/g, "</tr>\n").replace(/<\/td>\s*<td/g, '</td>%%<td');
        let div = document.createElement("div");
        div.innerHTML = text;
        target.textContent = div.textContent.replace(/%%/g, String.fromCharCode(9));
      }
      var currentFocus = document.activeElement;
      target.focus();
      target.setSelectionRange(0, target.value.length);
      var succeed;
      try {
        succeed = document.execCommand("copy");
      } catch (e) {
        succeed = false;
        console.error(e);
      }
      if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
      }

      if (isInput) {
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
      } else {
        target.textContent = "";
      }
      return succeed;
    }

    return {
      container,
      body,
      copyBtn,
      switcher,
      progress,
      setValue,
      clear,
      addProgress,
      close,
      showTable,
      copyToClipboard
    }
  }

  const createDataTable = () => {
    let table = document.createElement("table");
    table.id = "cwgGrabberTable";
    let thead = document.createElement("thead");
    let headTr = document.createElement("tr");
    let headTd1 = document.createElement("th");
    headTd1.textContent = "nick";
    let headTd2 = document.createElement("th");
    headTd2.textContent = "logy";
    let headTd3 = document.createElement("th");
    headTd3.textContent = "počet xWG";

    headTr.appendChild(headTd1);
    headTr.appendChild(headTd2);
    headTr.appendChild(headTd3);
    thead.appendChild(headTr);
    table.appendChild(thead);

    let tb = document.createElement("tbody");
    let myLogs = Array.from(logs);
    myLogs.sort((a, b) => a.userName.localeCompare(b.userName, 'cs', { sensitivity: "variant" }));
    let totalCwgSum = 0;
    myLogs.forEach(log => {
      let tr = document.createElement("tr");
      let td = document.createElement("td");

      let tdName = td.cloneNode(true);
      tdName.classList.add("name");
      tdName.textContent = log.userName;

      let tdLogs = td.cloneNode(true);
      tdLogs.classList.add("logs");
      let sortedLogs = log.logs.sort((a, b) => new Date(b.date) - new Date(a.date)).reverse();
      let cwgSum = 0;
      sortedLogs.forEach((l, i) => {
        let spanHead = document.createElement("span");
        spanHead.classList.add("log-info");
        spanHead.appendChild(document.createTextNode(`${l.type} (${l.date})`));
        let spanLog = document.createElement("span");
        spanLog.classList.add("log-text");
        spanLog.appendChild(document.createTextNode(l.text));
        tdLogs.appendChild(spanHead);
        tdLogs.appendChild(document.createElement("br"));
        tdLogs.appendChild(spanLog);
        cwgSum += l.cwgs;
        if ((i + 1) < sortedLogs.length) {
          tdLogs.appendChild(document.createElement("br"));
        }
      });

      let tdCwgs = td.cloneNode(true);
      tdCwgs.classList.add("cwgs");
      tdCwgs.textContent = cwgSum;
      totalCwgSum += cwgSum;

      tr.appendChild(tdName);
      tr.appendChild(tdLogs);
      tr.appendChild(tdCwgs);

      tb.appendChild(tr);
    });

    let lastRow = document.createElement("tr");
    let lastRowTd1 = document.createElement("td");
    lastRowTd1.colSpan = 2;
    lastRowTd1.textContent = "celkem:";
    let lastRowTd2 = document.createElement("td");
    lastRowTd2.textContent = totalCwgSum;
    lastRow.appendChild(lastRowTd1);
    lastRow.appendChild(lastRowTd2);
    tb.appendChild(lastRow);

    table.appendChild(tb);

    dom.overlay.showTable(table);
  }

  const createExcel = (logs, filename) => {

    if (!w.$JExcel) {
      dom.overlay.copyToClipboard(document.querySelector("#cwgGrabberOverlayBody.table table#cwgGrabberTable"));
      createError("Nepodařilo se načíst knihovnu pro tvorbu souboru XLSX. Tabulka byla zkopírována do schránky.");
      return false;
    }

    let cacheName = document.querySelector("#ctl00_ContentBody_CacheName");
    let cacheCode = document.querySelector("#ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode");
    let fileName = cacheName && cacheCode ? `${cacheCode.textContent}-${cacheName.textContent.replace(/\s/g, "_")}` : "CWG Grabber";

    let myLogs = Array.from(logs);
    let oneLine = false;
    let oneLineElm = document.querySelector("#singleLinesSwitch");
    if (oneLineElm) {
      oneLine = oneLineElm.checked;
    }

    var $JExcel = w.$JExcel;

    let excel = $JExcel.new("Calibri light 10 #333333");
    excel.addSheet(cacheCode ? cacheCode.textContent : "CWG Grabber");
    let headers = [
      {
        type: "nick",
        width: "auto"
      },
      {
        type: "datum",
        width: "auto"
      },
      {
        type: "typ",
        width: "auto"
      },
      {
        type: "log",
        width: 70
      },
      {
        type: "počet xWG",
        width: "auto",
      }
    ];
    let noOneLineTypes = ["datum", "typ"];

    if (!oneLine) {
      headers = headers.filter(item => {
        return !noOneLineTypes.includes(item.type)
      });
    }

    let formatHeader = excel.addStyle(JSON.stringify({
      border: "dotted #999999, dotted #999999, medium #666666, double #666666",
      font: "Calibri 12 #000000 B",
      align: "C T",
      fill: "#CECECE"
    }));

    let formatHeaderLast = excel.addStyle(JSON.stringify({
      border: "dotted #999999, medium #666666, medium #666666, double #666666",
      font: "Calibri 12 #000000 B",
      align: "C T",
      fill: "#CECECE"
    }));

    let formatHeaderFirst = excel.addStyle(JSON.stringify({
      border: "medium #666666, dotted #999999, medium #666666, double #666666",
      font: "Calibri 12 #000000 B",
      align: "C T",
      fill: "#CECECE"
    }));

    let formatNumber = excel.addStyle(JSON.stringify({
      border: "dotted #999999, medium #666666, none, thin #666666",
      font: "_ _ _ B",
      align: "R T"
    }));

    let formatLog = excel.addStyle(JSON.stringify({
      border: "dotted #999999, dotted #999999, none, thin #666666",
      align: "L T W"
    }));

    let formatName = excel.addStyle(JSON.stringify({
      border: "medium #666666, dotted #999999, none, thin #666666",
      font: "_ _ _ B",
      align: "L T"
    }));

    let formatCell = excel.addStyle(JSON.stringify({
      border: "dotted #999999, dotted #999999, none, thin #666666",
      align: "L T"
    }));

    let formatLastRow = excel.addStyle(JSON.stringify({
      border: "none, none, medium #666666, none",
      align: "R T"
    }));

    let formatLastRowSum = excel.addStyle(JSON.stringify({
      border: "none, none, medium #666666, none",
      font: "_ _ #CC0000 B",
      align: "R T"
    }));

    headers.forEach((cell, i) => {
      let format = i == 0 ? formatHeaderFirst : (i == (headers.length - 1) ? formatHeaderLast : formatHeader);
      excel.set(0, i, 0, cell.type, format);
      excel.set(0, i, undefined, cell.width);
    });

    let totalCwgSum = 0;
    let row = 1;
    myLogs.forEach(user => {
      if (oneLine) {
        user.logs.forEach(log => {
          excel.set(0, 0, row, user.userName, formatName);
          excel.set(0, 1, row, log.date, formatCell);
          excel.set(0, 2, row, log.type, formatCell);
          excel.set(0, 3, row, log.text, formatLog);
          excel.set(0, 4, row, log.cwgs, formatNumber);
          totalCwgSum += log.cwgs;
          row++;
        });
      } else {
        excel.set(0, 0, row, user.userName, formatName);
        let cwgSum = 0;
        let userLogRows = [];
        user.logs.forEach((log, index) => {
          userLogRows.push(`${log.date} (${log.type}):`);
          userLogRows.push(log.text);
          if (index < user.logs.length - 1) {
            userLogRows.push("------------");
          }
          cwgSum += parseInt(log.cwgs);
        });
        excel.set(0, 1, row, userLogRows.join("\n"), formatLog);
        excel.set(0, 2, row, cwgSum, formatNumber);
        totalCwgSum += cwgSum;
        row++;
      }
    });

    let maxCols = oneLine ? 5 : 3;
    for (let i = 0; i < maxCols; i++) {
      let col = { 5: "E", 3: "C" }
      let text = i == maxCols - 2 ? "celkem:" : i == maxCols - 1 ? `=SUM(${col[maxCols]}2:${col[maxCols]}${row})|${totalCwgSum}` : "";
      excel.set(0, i, row, text, i == maxCols - 1 ? formatLastRowSum : formatLastRow);
    }

    excel.generate(`${fileName}.xlsx`);
  }

  const init = () => {
    document.body.appendChild(dom.container);
  }

  const dom = {
    container: document.createElement("div"),
    getLogs: document.createElement("button"),
    fragment: document.createElement("div"),
    overlay: createOverlay()

  }
  let totalPages = 0;
  let totalLogs = 0;
  let logs = [];

  const css = [
    `#cwgGrabberButtonContainer {
      position: fixed;
      top: 90vh;
      right: 10px;
      padding: 5px;
      background-color: rgba(0, 95, 47, 0.3);
      border-radius: 3px;
      z-index: 99900;
    }`,
    `#cwgGrabberButtonContainer button {
      cursor: pointer;
    }`,
    `#cwgGrabberOverlayContainer {
      width: 100vw;
      height: 100vh;
      position: fixed;
      z-index: 99999;
      top: 0;
      left: 0;
      background-color: rgba(0,0,0,0.75);
      display: flex;
      justify-content: center;
      align-items: center;
    }`,
    `#cwgGrabberOverlayBody {
      width: 400px;
      background-color: white;
      border-radius: 5px;
      padding: 10px;
      text-align: center;
      max-height: 90vh;
    }`,
    `#cwgGrabberOverlayBody.table {
      width: 75vw;
      position: relative;
    }`,
    `#cwgGrabberOverlayBody h3 {
      margin-bottom: 25px;
    }`,
    `#cwgGrabberOverlayBody .meter {
      width: 390px;
      height: 14px;
      position: relative;
      background: rgb(183, 224, 177);
      border-radius: 17px;
      padding: 5px;
      box-shadow: inset 0 1px 1px rgba(0,0,0,0.5);
    }`,
    `#cwgGrabberOverlayBody .meter > span {
      display: block;
      height: 100%;
      border-radius: 7px;
      background-color: rgb(43,194,83);
      background-image: linear-gradient(
        center bottom,
        rgb(43,194,83) 37%,
        rgb(84,240,84) 69%
      );
      box-shadow:
        inset 0 2px 9px  rgba(255,255,255,0.3),
        inset 0 -2px 6px rgba(0,0,0,0.4);
      position: relative;
      overflow: hidden;
      transition: width 500ms ease 0s;
    }`,
    `#cwgGrabberOverlayBody .meter > span:after, .animate > span > span {
      content: "";
      position: absolute;
      top: 0; left: 0; bottom: 0; right: 0;
      background-image:
        linear-gradient(
          -45deg,
          rgba(255, 255, 255, .2) 25%,
          transparent 25%,
          transparent 50%,
          rgba(255, 255, 255, .2) 50%,
          rgba(255, 255, 255, .2) 75%,
          transparent 75%,
          transparent
        );
      z-index: 1;
      background-size: 50px 50px;
      animation: move 2s linear infinite;
      border-radius: 17px;
      overflow: hidden;
    }`,
    `#cwgGrabberOverlayBody .animate > span:after {
      display: none;
    }`,
    `@keyframes move {
      0% {
        background-position: 0 0;
      }
      100% {
        background-position: 50px 50px;
      }
    }`,
    `#cwgGrabberOverlayBody .tableContainer {
      overflow: auto;
      max-height: calc(90vh - 90px);
      margin-bottom: 10px;
      background-color: #dddee1;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table {
      border: 2px solid black;
      border-collapse: collapse;
      text-align: left;
      margin: 0;
      width: 100%;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table td {
      border: 1px solid #666;
      padding: 10px;
      min-width: 2em;
      vertical-align: top;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table td.name {
      font-weight: bold;
      border-right-style: dotted;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table td.logs {
      border-left-style: dotted;
      border-right-style: dotted;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table td.cwgs {
      border-left-style: dotted;
      text-align: right;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table td.logs .log-info:not(:first-child) {
      display: inline-block;
      margin-top: 10px;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table thead tr {
      border-bottom: double 3px #666;
      background-color: #999;
      color: whitesmoke;
      font-weight: bold;
      text-align: center;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table thead tr th {
      border: 1px dotted whitesmoke;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table tbody tr:last-child {
      border-top: double 3px #666;
      background-color: #999;
      color: whitesmoke;
      text-align: right;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table tbody tr:last-child td:first-child {
      border-right-style: dotted;
      border-right-color: whitesmoke;
      font-weight: bold;
    }`,
    `#cwgGrabberOverlayBody .tableContainer table tbody tr:last-child td:last-child {
      border-left-style: dotted;
      border-left-color: whitesmoke;
      font-weight: bold;
    }`,
    `#cwgGrabberOverlayBody .close-button {
      position: absolute;
      top: 8px;
      right: 10px;
      font-size: 34px;
      color: #b60707;
      border-radius: 50%;
      background-color: #ffe0e0;
      width: 40px;
      height: 40px;
      line-height: 1;
      padding-top: 2px;
      box-sizing: border-box;
      cursor: pointer;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch {
      width: 90px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      display: inline-block;
      margin-right: 20px;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-checkbox {
      display: none;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-label {
      display: block;
      overflow: hidden;
      cursor: pointer;
      border: 2px solid #999999;
      border-radius: 20px;
      position: relative;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-inner {
      display: block;
      width: 200%;
      margin-left: -100%;
      transition: margin 0.3s ease-in 0s;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-inner:before,
    #cwgGrabberOverlayBody.table .onoffswitch-inner:after {
      display: block;
      float: left;
      width: 50%;
      height: 25px;
      padding: 0;
      line-height: 25px;
      font-size: 14px;
      color: white;
      font-weight: bold;
      box-sizing: border-box;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-inner:before {
      content: "ano";
      padding-left: 10px;
      background-color: #00A85F;
      color: #FFFFFF;
      text-align: left;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-inner:after {
      content: "ne";
      padding-right: 10px;
      background-color: #EEEEEE;
      color: #999999;
      text-align: right;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-switch {
      display: block;
      width: 15px;
      height: 15px;
      margin: 3px;
      background: #FFFFFF;
      position: absolute;
      top: 0;
      bottom: 0;
      right: 61px;
      border: 2px solid #999999;
      border-radius: 20px;
      transition: all 0.3s ease-in 0s;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-checkbox:checked+.onoffswitch-label .onoffswitch-inner {
      margin-left: 0;
    }`,
    `#cwgGrabberOverlayBody.table .onoffswitch-checkbox:checked+.onoffswitch-label .onoffswitch-switch {
      right: 0px;
    }`,
    `#cwgGrabberOverlayBody.table label.textlabel {
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      cursor: pointer;
    }`,
    `#cwgGrabberOverlayBody .cwgGrabberLastLine {
      display: flex;
      gap: 1rem;
      justify-content: center;
      align-items: center;
    }`
  ];

  const style = document.createElement('style');
  document.head.appendChild(style);
  css.forEach(rule => {
    style.sheet.insertRule(rule, style.sheet.cssRules.length);
  });

  tokenPromise.then(usToken => {
    console.info("CWG Grabber: user token found!");
    ut = usToken;
    dom.container.id = "cwgGrabberButtonContainer";

    dom.getLogs.textContent = "Získat počty xWG";
    dom.getLogs.addEventListener("click", () => {
      if (!dom.getLogs.disabled) {
        dom.getLogs.disabled = true;
        start();
      }
    });

    dom.container.appendChild(dom.getLogs);
    console.info("Lazy load started!");
    lazyLoad(`https://gpx.littleband.cz/cwggrabber/jszip.min.js`)
      .then(() => {
        return lazyLoad(`https://gpx.littleband.cz/cwggrabber/FileSaver.min.js`);
      })
      .then(() => {
        return lazyLoad(`https://gpx.littleband.cz/cwggrabber/myexcel.min.js`);
      })
      .then(() => {
        console.info("CWG Grabber: lazy load completed.")
        toExcel = true;
        if (dom.overlay.copyBtn) {
          dom.overlay.copyBtn.textContent = "Vytvořit soubor Excelu.";
          addCopyListener(dom.overlay.copyBtn);
        }
        init();
      })
      .catch(err => {
        console.info("Lazy loading failed!");
        console.error(err);
        init();
      });
  }).catch(() => {
    console.info("CWG Grabber: user token not found!");
  });
}());
